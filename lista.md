## Apanhado de links 

### Artigos

* https://moxie.org/blog/we-should-all-have-something-to-hide/
* https://encripta.org/dicas_e_tutoriais_seguranca/
* https://privacysos.org/blog/so-you-think-you-have-nothing-to-hide/
* https://www.theguardian.com/technology/blog/2013/jun/14/nsa-prism
* https://www.aclu.org/blog/national-security/you-may-have-nothing-hide-you-still-have-something-fear
* https://www.theguardian.com/commentisfree/2015/sep/17/ahmed-mohamed-istandwithahmed-counter-terror-andrew-parker
* https://www.commondreams.org/news/2015/02/13/privacy-critical-human-freedom-snowden-poitras-and-greenwald-talk-nsa
* https://www.gnu.org/philosophy/surveillance-vs-democracy.html
* https://theintercept.com/2016/04/28/new-study-shows-mass-surveillance-breeds-meekness-fear-and-self-censorship/
* https://stallman.org/
* https://www.theguardian.com/technology/2011/mar/17/us-spy-operation-social-networks
* https://outraspalavras.net/posts/comecou-a-censura-politica-no-google/
* https://hackernoon.com/things-you-need-to-know-about-facebook-and-mass-manipulation-bed5c92806f1?gi=caaa49522f09
* https://www.cartacapital.com.br/blogs/outras-palavras/o-novo-estado-da-vigilancia-global
* https://www.theguardian.com/world/2013/dec/02/turing-snowden-transatlantic-pact-modern-surveillance
* https://outraspalavras.net/brasil/o-brasil-no-epicentro-da-guerra-hibrida/
* https://www.scientificamerican.com/article/will-democracy-survive-big-data-and-artificial-intelligence/
* https://www.theguardian.com/world/interactive/2013/nov/01/snowden-nsa-files-surveillance-revelations-decoded#section/6
* https://www.theguardian.com/technology/2014/jun/29/facebook-users-emotions-news-feeds
* https://www.zerohedge.com/news/2014-07-09/pentagon-admits-spending-millions-study-social-media-manipulation
* https://www.theguardian.com/technology/2016/dec/16/google-autocomplete-rightwing-bias-algorithm-political-propaganda
* https://en.wikipedia.org/wiki/Hybrid_warfare
* https://www.thestar.com/opinion/contributors/2017/12/15/how-artificial-intelligence-will-be-used-for-social-control.html
* https://outraspalavras.net/posts/controle-social-do-grande-irmao-ao-big-data/
* https://outraspalavras.net/posts/gerindo-a-desordem-rumo-ao-estado-de-controle-global/
* https://outraspalavras.net/posts/pentagono-pesquisa-o-controle-social/
* https://outraspalavras.net/posts/o-fantasma-do-ultra-capitalismo/

### Videos

* https://www.ted.com/talks/glenn_greenwald_why_privacy_matters

* https://www.ted.com/talks/jennifer_granick_how_the_us_government_spies_on_people_who_protest_including_you

* [Xploit - internet sob ataque -- doc](https://www.youtube.com/watch?v=A4_gJm0UmRA)

  

